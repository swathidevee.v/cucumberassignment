package com.testng1;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/feature3",
glue= {"com.testng1"})
public class TestngRunner extends AbstractTestNGCucumberTests {

}
