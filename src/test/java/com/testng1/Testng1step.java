package com.testng1;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Testng1step {
	static WebDriver driver=null;
	@Given("open the browser")
	public void open_the_browser() {
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.get("https://demowebshop.tricentis.com/");
	}
	
	
	
	@When("Under login the user enters mailId {string} and password {string}")
	public void under_login_the_user_enters_mail_id_and_password(String string, String string2) {
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys(string);
		driver.findElement(By.id("Password")).sendKeys(string2);
	}



	@When("The user click on login button")
	public void the_user_click_on_login_button() {
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Then("The user is logged in into his DemoWebshop account")
	public void the_user_is_logged_in_into_his_demo_webshop_account() {
		
		System.out.println("Home page");
	}

	@Then("The user clicks on logout")
	public void the_user_clicks_on_logout() {
		driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
	}

	@Then("The user is taken to the Home page of DemoWebShop")
	public void the_user_is_taken_to_the_home_page_of_demo_web_shop() {
		System.out.println("Home page");
	}

	@Then("Close the Broser")
	public void close_the_broser() {
		driver.close();
	}
}
