Feature: DemoWebShop login feature

Scenario Outline: The user enter into the DemoWebShop site and login into the home page

Given open the browser
When Under login the user enters mailId "<Id>" and password "<Pass>"
And The user click on login button
Then The user is logged in into his DemoWebshop account 
And  The user clicks on logout
Then The user is taken to the Home page of DemoWebShop
And Close the Broser

    Examples: 
      | Id                     | Pass     | 
      | swathivenkat@gmail.com |swathi123 | 
      | swathidevee@gmail.com  | swathi123|
